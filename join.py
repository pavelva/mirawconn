import argparse
import glob
import pandas as pd


parser = argparse.ArgumentParser()
parser.add_argument('-p','--path', help= 'path to input files', default='./data')
parser.add_argument('-o','--output', help= 'path to output files', default='./input')
parser.add_argument('-f','--filter', help= 'filter by MFE? yes/no', default='yes')
args = parser.parse_args()

files=glob.glob(args.path+'/**/*.positiveTargetSites.csv',recursive=True)


li=[]

for file in files:
    #print(file)
    try:
        df = pd.read_csv(file,sep='\t')
    except pd.errors.EmptyDataError:
        pass
    li.append(df)

data = pd.concat(li)
print('Original size: ',len(data))


data = data[data['Prediction'] >= 0.985] 
#data = data[data['MFE'] < -25]  
print('Size after MFE and probability filters: ',len(data))
#df_RRAGD = data[data['GeneName'] == 'ENSG00000025039___RRAGD_HUMAN___6-89364616-89368055n']

canonical = data[data.Canonical == "PITA: canonnical"]
noncanonical = data[data.Canonical == "PITA: noncanonical"]
print('Canonical subset: ',len(canonical),',  Noncanonical substet: ', len(noncanonical),', Sum: ', len(canonical)+len(noncanonical))

data.to_csv(args.output+'/output.csv', sep='\t')
#canonical.to_csv(args.output+'/canonical.csv', sep='\t')
#noncanonical.to_csv(args.output+'/noncanonical.csv', sep='\t')
    