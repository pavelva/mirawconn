#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  2 17:26:55 2021

@author: pavelvazquez
"""
import pandas as pd
import gffpandas.gffpandas as gffpd
import numpy as np
import argparse
import re




parser = argparse.ArgumentParser()
parser.add_argument('-d','--derive', help= 'path to input', default='./occurs/miRs_hsa.21pt1.gff3')
parser.add_argument('-i','--input', help= 'path to input', default='./occurs/miriad_intragenic_micrornas_v.12_2012.tsv')
parser.add_argument('-r','--result', help= 'path to input', default='./input/output.csv')
parser.add_argument('-o','--output', help= 'path to output', default='./occurs')



args = parser.parse_args()


##LOAD THE DATA##



####    STEP1:parse the pre-miRNA and the miRNA ########

data = pd.read_csv(args.derive,skiprows=11,sep='\t')
df = pd.DataFrame(data)
df['#'] = df['#'].astype(str)
l=[]
for i in range(len(df)):
    l.append(re.findall('(?<=Name=).*$', df.iloc[i]['#']))
l=pd.DataFrame(l)
l.columns =['code'] 
l[['miRNA','Derives']] = l.code.str.split(";",expand=True) 
l['Derives']=l['Derives'].str.split('\=').str[-1].str.strip()
df0 = l.drop('code', 1)
derives=dict(zip(l.Derives, l.miRNA))

####    STEP2:parse if it is in or exo #######

data1 = pd.read_csv(args.input,sep='\t')
df1 = pd.DataFrame(data1)
#names=df1.intragenic_type.unique()



#### STEP3:check which one are in or exo for the joined files #####


data2 = pd.read_csv(args.result,skiprows=0,sep='\t')
df2 = pd.DataFrame(data2)
df2['miRNA_name']=l['miRNA'].str.split('\_').str[-1].str.strip()


####STEP 4 join###
df1['pre_miRNA']=df1['mirbase_accession'].map(derives)
derives1=dict(zip(df1['pre_miRNA'], df1['intragenic_type']))
df2['intragenic_type']=df2['miRNA_name'].map(derives1)

intronic = df2[df2.intragenic_type == "intronic"]
exonic = df2[df2.intragenic_type == "exonic"]
print('Intronic subset: ',len(intronic),',  Exonic substet: ', len(exonic),', Sum: ', len(exonic)+len(intronic))


intronic.to_csv(args.output+'/intronic.csv', sep='\t')
exonic.to_csv(args.output+'/exonic.csv', sep='\t')

