#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 17:54:41 2021

@author: pavelvazquez
"""


import pandas as pd
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from networkx.algorithms import bipartite


df = pd.read_csv('./output.csv',sep='\t')


df = df.iloc[111000:]

count_miRNA = df.pivot_table(index=['miRNA'], aggfunc='size').reset_index()
count_miRNA.columns.values[1] = "count"
count_gene = df.pivot_table(index=['GeneName'], aggfunc='size').reset_index()
count_gene.columns.values[1] = "count"
count_miRNA=count_miRNA.sort_values(by=['count'],ascending=False)

df_miR=df


#Count of unique values for the modified matrix df_miR
count_miRNA_E = df_miR.pivot_table(index=['miRNA'], aggfunc='size').reset_index()
count_miRNA_E.columns.values[1] = "count"
count_gene_E = df_miR.pivot_table(index=['GeneName'], aggfunc='size').reset_index()
count_gene_E.columns.values[1] = "count"
count_miRNA_E=count_miRNA_E.sort_values(by=['count'],ascending=False)

#Generates the dataframe for df, complete matrix
allm= (df.pivot_table(index=['miRNA','GeneName'], aggfunc='size')).reset_index()

matrix=pd.DataFrame(np.zeros((count_miRNA.shape[0],count_gene.shape[0])),index =[count_miRNA['miRNA']], columns=[count_gene['GeneName']]) 
for j in range(0,len(allm)):
    matrix.loc[allm.iloc[j,0],allm.iloc[j,1]]=1

allm_miR= (df_miR.pivot_table(index=['miRNA','GeneName'], aggfunc='size')).reset_index()
matrix_miR=pd.DataFrame(np.zeros((count_miRNA.shape[0],count_gene.shape[0])),index =[count_miRNA['miRNA']], columns=[count_gene['GeneName']]) 

for j in range(0,len(allm_miR)):
    matrix_miR.loc[allm_miR.iloc[j,0],allm_miR.iloc[j,1]]=1

df_count=count_miRNA
df_count_E=count_miRNA_E


print('calculating...')
B = nx.Graph()
B.add_nodes_from(df['miRNA'], bipartite=0)
B.add_nodes_from(df['GeneName'], bipartite=1)



B.add_weighted_edges_from(
    [(row['GeneName'], row['miRNA'], 1) for idx, row in df.iterrows()], 
    weight='MFE')

pos = {node:[0, i] for i,node in enumerate(df['GeneName'])}
pos.update({node:[1, i] for i,node in enumerate(df['miRNA']

nx.draw(B, pos=pos, 
        node_color='lightgreen', 
        node_size=2500,
        with_labels=True)
print('ploting...')

plt.savefig('./network.png',dpi=300)
    