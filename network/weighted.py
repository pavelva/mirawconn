#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  5 20:03:22 2021

@author: pavelvazquez
"""
import pandas as pd
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from networkx.algorithms import bipartite


df = pd.read_csv('./output.csv',sep='\t')
df['GeneName'] = df['GeneName'].str[18:]
df['miRNA'] = df['miRNA'].str.split('_').str[0]
#df=df.sample(n=300, random_state=1)
#df = df.iloc[100000:]
A=[(df.GeneName.iloc[0],df.miRNA.iloc[0])]
for i in range(1,df.shape[0]):
    
    C=(df.GeneName.iloc[i],df.miRNA.iloc[i])
    A.append(C)






B = nx.Graph()
B.add_nodes_from(df['miRNA'], bipartite=0)
B.add_nodes_from(df['GeneName'], bipartite=1)

B.add_edges_from(A)



top = nx.bipartite.sets(B)[0]
pos = nx.bipartite_layout(B, top)




nx.draw(B, pos=pos,
        node_size=0.5,
        with_labels=True,
        node_color='steelblue',
        edge_color='salmon',
        width=0.1,
        font_size=2
        )


plt.savefig('./network.png',dpi=300)
