This work is an extension of miRAW Deep Learning software made by Albert Pla,Xiangfu Zhong,Simon Rayner (https://doi.org/10.1371/journal.pcbi.1006185)

I do not own this software. For the original code please visit: https://bitbucket.org/bipous/workspace/projects/MIRAW

How to run the script: 

Run ./runME.sh

Input needed is 3.fa and mirs.fad files in the "input" folder

Output is writen in the generated "output" folder