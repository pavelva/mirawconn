#!/usr/bin/env python3




import pandas as pd
import numpy as np
import argparse
import os
import matplotlib.pyplot as plt


# Path parser
parser = argparse.ArgumentParser()
parser.add_argument('-i','--input', help= 'path to input', default='./input/')
parser.add_argument('-o','--output', help= 'path to the output file (default ./connectivity.png)', default='./connectivity.png')
parser.add_argument('-p','--parameter', help= '1. Unfiltered file 2. Filtered file', default='1')

args = parser.parse_args()


##LOAD THE DATA##
print('Reading ' + args.input)


csvs = [x for x in os.listdir(args.input) if x.endswith('.csv')]
# stats.csv -> stats
files = [os.path.splitext(os.path.basename(x))[0] for x in csvs]
df = {}
for i in range(len(files)):
    df[i] = pd.read_csv(args.input+csvs[i],sep='\t')
 




#data = pd.read_csv(args.input,skiprows=0,sep='\t')
#df = pd.DataFrame(data)



    count_miRNA = df[i].pivot_table(index=['miRNA'], aggfunc='size').reset_index()
    count_miRNA.columns.values[1] = "count"
    count_gene = df[i].pivot_table(index=['GeneName'], aggfunc='size').reset_index()
    count_gene.columns.values[1] = "count"
    count_miRNA=count_miRNA.sort_values(by=['count'],ascending=False)

    df_miR=df[i]


    #Count of unique values for the modified matrix df_miR
    count_miRNA_E = df_miR.pivot_table(index=['miRNA'], aggfunc='size').reset_index()
    count_miRNA_E.columns.values[1] = "count"
    count_gene_E = df_miR.pivot_table(index=['GeneName'], aggfunc='size').reset_index()
    count_gene_E.columns.values[1] = "count"
    count_miRNA_E=count_miRNA_E.sort_values(by=['count'],ascending=False)

    #Generates the dataframe for df, complete matrix
    allm= (df[i].pivot_table(index=['miRNA','GeneName'], aggfunc='size')).reset_index()

    #Generates the dataframe for df, complete matrix
    matrix=pd.DataFrame(np.zeros((count_miRNA.shape[0],count_gene.shape[0])),index =[count_miRNA['miRNA']], columns=[count_gene['GeneName']]) 
    for j in range(0,len(allm)):
        matrix.loc[allm.iloc[j,0],allm.iloc[j,1]]=1

    allm_miR= (df_miR.pivot_table(index=['miRNA','GeneName'], aggfunc='size')).reset_index()
    matrix_miR=pd.DataFrame(np.zeros((count_miRNA.shape[0],count_gene.shape[0])),index =[count_miRNA['miRNA']], columns=[count_gene['GeneName']]) 

    for j in range(0,len(allm_miR)):
        matrix_miR.loc[allm_miR.iloc[j,0],allm_miR.iloc[j,1]]=1

    df_count=count_miRNA
    df_count_E=count_miRNA_E

    #Counts the # of nodes that have an specific # of connections
    count =df_count.pivot_table(index=['count'], aggfunc='size').reset_index()
    count.columns.values[0] = "# of connections"
    count.columns.values[1] = "# of nodes"
    count_E =df_count_E.pivot_table(index=['count'], aggfunc='size').reset_index()
    count_E.columns.values[0] = "# of connections"
    count_E.columns.values[1] = "# of nodes"

    #creating a third dataframe but eliminating the most connected node
    #count_rm=count_miRNA_E[~count_miRNA_E.miRNA.str.contains(count_miRNA['miRNA'].iloc[0])]
                
    #count_V =count_rm.pivot_table(index=['count'], aggfunc='size').reset_index()
    #count_V.columns.values[0] = "# of connections"
    #count_V.columns.values[1] = "# of nodes"


    ##Stahistics#
    #from scipy import stats
    #stats.ks_2samp(count['# of nodes'],count_E['# of nodes'])

    countP=count.sort_values(by=['# of connections'],ascending=False)
    ###Plot#### 
    n=5

    x=countP['# of connections'].values.tolist()
    x=np.asarray(x).astype(float)
    y=countP['# of nodes'].values.tolist()
    y=np.asarray(y).astype(float)

    #count_EP=count_E.sort_values(by=['# of connections'],ascending=False)
    #count_EP=count_EP.div(count_EP['# of connections'].sum())
    #x_E=count_EP['# of connections'].values.tolist()
    #x_E=np.asarray(x_E).astype(float)
    #y_E=count_EP['# of nodes'].values.tolist()
    #y_E=np.asarray(y_E).astype(float)

    #count_EV=count_V.sort_values(by=['# of connections'],ascending=False)
    #count_EP=count_EP.div(count_EP['# of connections'].sum())
    #x_V=count_EV['# of connections'].values.tolist()
    #x_V=np.asarray(x_V).astype(float)
    #y_V=count_EV['# of nodes'].values.tolist()
    #y_V=np.asarray(y_V).astype(float)


    poly = np.polyfit(x,y,n)
    y_p = np.poly1d(poly)(x)


    #poly_E = np.polyfit(x_E,y_E,n)
    #y_p_E = np.poly1d(poly_E)(x_E)


    #poly_V = np.polyfit(x_V,y_V,n)
    #y_p_V = np.poly1d(poly_V)(x_V)
    #plt.figure()
    colors=['b','g','r','c','m','y','k']
    print('Plotting.....')
    
    count=count.sort_values(by=['# of nodes'],ascending=False)
    #count_smooth = count.reindex().interpolate(method='polynomial', order=6)
    #print(count_smooth.shape)
    
    
    #ax = countP.plot(kind="scatter", y="# of nodes",x="# of connections", color=colors[i],marker="s", label=files[i], s=25)
    #ax.set_ylabel("# of nodes")
    #ax.set_xlabel("Degree <k>")
    ##SCATTER##
    plt.xlabel("# of nodes")
    plt.ylabel("Degree <k>")

    plt.plot(countP['# of connections'], countP['# of nodes'],'o',color=colors[i],label=files[i],markersize=2)
    plt.legend()
    plt.plot(x,y_p, color=colors[i])
    
countP.to_csv('./output.csv', sep='\t')
#count_EP.plot(kind="scatter",y="# of nodes",x="# of connections", color="b", marker="o",s=23, label="Reduced", ax=ax)
#plt.plot(x_E,y_p_E, color="b")
#count_V.plot(kind="scatter",y="# of nodes",x="# of connections", color="r", marker="o",s=10, label="Erased", ax=ax)
#plt.plot(x_V,y_p_V, color="r")


#plt.show()
plt.savefig(args.output,dpi=300)
print('Connectivity plot created in ' + args.output)
#count_miRNA_E.to_csv('./miRNA_names.csv', sep='\t')




