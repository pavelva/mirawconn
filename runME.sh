#!/usr/bin/env bash

mkdir ./unified

python3 miRAWbatch.py -e result -o ./output/ -d ./bestModel.bin -c pita -u ./unified/unifiedFile.tsv -m ./input/mirs.fa -3 ./input/3.fa -j ./miRAW.jar -t 1

chmod 757 ./output/result.sh
./output/result.sh

python3 connectivity.py -p ./output/result/targetPredictionOutput.filtered.csv
