#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  2 16:13:03 2021

@author: pavelvazquez
"""
import pandas as pd

import argparse
import os
import matplotlib.pyplot as plt


# Path parser
parser = argparse.ArgumentParser()
parser.add_argument('-i','--input', help= 'path to input', default='./histogram_input/')
parser.add_argument('-o','--output', help= 'path to the output file (default ./histrogram.png)', default='./histogram.png')
parser.add_argument('-p','--parameter', help= '1. Unfiltered file 2. Filtered file', default='1')

args = parser.parse_args()


##LOAD THE DATA##
print('Reading ' + args.input)


csvs = [x for x in os.listdir(args.input) if x.endswith('.csv')]
# stats.csv -> stats
files = [os.path.splitext(os.path.basename(x))[0] for x in csvs]
df = {}
for i in range(len(files)):
    df[i] = pd.read_csv(args.input+csvs[i],sep='\t')
    x=df[i]['MFE']
    plt.hist(x, density=True, bins=50,label=files[i])
    plt.legend()
    plt.ylabel('Count')
    plt.xlabel('MFE');
    
    
plt.savefig(args.output,dpi=300)
print('Connectivity plot created in ' + args.output)